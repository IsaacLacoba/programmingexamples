#include "Node.hpp"

void Node::setVisited(bool visited) {
    _visited = visited;
}

bool Node::isVisited() {
    return _visited;
}

int Node::getId() {
    return _id;
}

bool Node::equals(Node* node) {
    Debug::Log::d("BFSGraph", "equals current: %d  comparable: %d", getId(), node->getId());

    return node->getId() == _id;
}
