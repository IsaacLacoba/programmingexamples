#include "BFSStrategy.hpp"

void BFSStrategy::copyNextNodes(Node* nodeToCopy) {
    Debug::Log::d("BFSGraph", "copying nodes: %d", nodeToCopy->neighbours.size());
    for_each(nodeToCopy->neighbours.begin(), nodeToCopy->neighbours.end(),
	     [this](Node* node) {
		 //If no duplicate nodes are presented in frontier, insert it
		 if (!node->isVisited()) {
		     frontier.push(node);
		 }
		 Debug::Log::d("BFSGraph", "NodeId: %d frontier size: %d", node->getId(), frontier.size());
	     });
}

Node* BFSStrategy::traverse(Node* initial, Node* destiny) {
    Node* currentNode = initial;
    initial->setVisited(true);

    if (!frontier.empty()) {
	while (frontier.front()->isVisited())
	    frontier.pop();
	currentNode = frontier.front();
	frontier.pop();
    }

    Debug::Log::d("BFSGraph", "current node: %d", currentNode->getId());

    if (currentNode->equals(destiny)) {
	Debug::Log::d("BFSGraph", "destination found returning: %d", currentNode->getId());
	return currentNode;
    }

    copyNextNodes(currentNode);
    currentNode->parent = traverse(currentNode, destiny);

    Debug::Log::d("BFSGraph","currentNodeId: %d parentId: %d", currentNode->getId(), currentNode->parent->getId());

    return currentNode;
}
