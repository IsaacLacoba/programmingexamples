#include "DFSStrategy.hpp"

void DFSStrategy::copyNextNodes(Node* nodeToCopy) {
    Debug::Log::d("BFSGraph", "copying nodes: %d", nodeToCopy->neighbours.size());
    //We iterate from the end to the beginning so the left child be the first
    for_each(nodeToCopy->neighbours.rbegin(), nodeToCopy->neighbours.rend(),
	     [this](Node* node) {
		 //If no duplicate nodes are presented in frontier, insert it
		 if (!node->isVisited()) {
		     frontier.push(node);
		 }
		 Debug::Log::d("BFSGraph", "NodeId: %d frontier size: %d", node->getId(), frontier.size());
	     });
}

Node* DFSStrategy::traverse(Node* initial, Node* destiny) {
    Node* currentNode = initial;
    initial->setVisited(true);

    if (!frontier.empty()) {
	while (frontier.top()->isVisited())	    
	    frontier.pop();
	currentNode = frontier.top();
	frontier.pop();
    }

    if (currentNode->equals(destiny)) {
	Debug::Log::d("BFSGraph", "destination found returning: %d", currentNode->getId());
	return currentNode;
    }
    copyNextNodes(currentNode);
    currentNode->parent = traverse(currentNode, destiny);

    Debug::Log::d("BFSGraph","currentNodeId: %d parentId: %d", currentNode->getId(), currentNode->parent->getId());

    return currentNode;
}
