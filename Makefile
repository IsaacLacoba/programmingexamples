
CPP := clang++-3.9
INCLUDE_DIR := include
DEBUG_LEVEL := "DEBUG_LEVEL=2"

BUILD_DIR := ./build

DEFINES := $(DEBUG_LEVEL)

OBJ_FILES := $(shell find -type f -name '*.cpp' ! -name "BFSGraph.cpp")

all:
	$(CPP) -std=c++11 -I $(addsuffix /, $(INCLUDE_DIR)) -D$(DEFINES) $(OBJ_FILES) BFSGraph.cpp -o $(BUILD_DIR)/BFSGraph.out
