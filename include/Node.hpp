/*
 * This source file is part of Samples
 * Copyright (C) 2019  Isaac Lacoba Molina
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
*/

#ifndef __NODE__
#define __NODE__
#include "Logger.hpp"

#include <vector>
#include <utility>

class Node {
    int _id;
    bool _visited;

    friend class Graph;
public:
    std::vector<Node*> neighbours;
    Node* parent = nullptr;

    Node(int id, std::vector<Node*> adjacent = {}, bool visited = false):
        _id(id), neighbours(adjacent), _visited(visited) {}

    void setVisited(bool visited);

    bool isVisited();

    int getId();

    bool equals(Node* node);
};

using Edge = std::pair<Node, Node>;

#endif
