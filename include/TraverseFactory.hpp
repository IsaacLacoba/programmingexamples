/*
 * This source file is part of Samples
 * Copyright (C) 2019  Isaac Lacoba Molina
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
*/

#ifndef __TRAVERSE__
#define __TRAVERSE__

#include <algorithm>

#include "TraverseStrategy.hpp"
#include "BFSStrategy.hpp"
#include "DFSStrategy.hpp"


class TraverseFactory {
public:
    static TraverseStrategy* getDefaultStrategy() {
	return new BFSStrategy();
    }

    static TraverseStrategy* getDFSStrategy() {
	return new DFSStrategy();
    }
};

#endif
