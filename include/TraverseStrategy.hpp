/*
 * This source file is part of AlgoritmSample
 * Copyright (C) 2019  Isaac Lacoba Molina
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
*/
#ifndef __STRATEGY__
#define __STRATEGY__
#include "Node.hpp"

class TraverseStrategy {
public:
    virtual ~TraverseStrategy() = default;

    virtual Node* traverse(Node* initial, Node* destiny) = 0;
    virtual void copyNextNodes(Node* nodeToCopy) = 0;
};
#endif // __STRATEGY__
