/*
 * This source file is part of Samples
 * Copyright (C) 2019  Isaac Lacoba Molina
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
*/

#ifndef __GRAPH__
#define __GRAPH__

#include "Node.hpp"
#include "TraverseStrategy.hpp"
#include "TraverseFactory.hpp"

#include <algorithm>

class Graph {
    std::vector<Node> _nodes;
    std::vector<Edge> _edges;

    TraverseStrategy* strategy;

public:
    Graph(std::vector<Node> nodes, std::vector<Edge> edges, TraverseStrategy* argStrategy = TraverseFactory::getDefaultStrategy()):
        _nodes(nodes), _edges(edges), 	strategy(argStrategy) {}

    ~Graph();

    Node* traverse(Node* initial, Node* destiny);

    void cleanNodes();

};

#endif
