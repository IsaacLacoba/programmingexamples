#include "Graph.hpp"

Graph::~Graph() {
    delete strategy;
}
Node* Graph::traverse(Node* initial, Node* destiny) {
    cleanNodes();
    return strategy->traverse(initial, destiny);
}

void Graph::cleanNodes() {
    std::for_each(_nodes.begin(), _nodes.end(), [](Node& node) { node._visited = false; });
}
