#include <iostream>
#include <ctime>

#include "Logger.hpp"
#include "Node.hpp"
#include "Graph.hpp"
#include "TraverseFactory.hpp"

int main(int argc, char *argv[])
{
    double duration;
    std::clock_t start;
    start = std::clock();

    Node node0 = {0};
    Node node1 = {1};
    Node node2 = {2};
    Node node3 = {3};

    node0.neighbours = {&node1, &node2};
    node1.neighbours = {&node2};
    node2.neighbours = {&node0, &node3};
    node3.neighbours = {&node3};

    Graph graph = {
	{node0, node1, node2, node3},
	{ {node0, node1},
	      {node0, node2},
	      {node1, node2},
	      {node2, node3},
	      {node3, node3}
	}
    };

    Debug::Log::i("BFSGraph", "lets found a solution!");

    Node* solution = graph.traverse(&node0, &node3);

    Debug::Log::i("BFSGraph", "solution found!");

    while(solution != nullptr) {
    	Debug::Log::i("BFSGraph", "Node: %d", solution->getId());
    	solution = solution->parent;
	if (solution != nullptr) {
	    Debug::Log::i("BFSGraph"," -> ");
	}
    }

    duration = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;

    Debug::Log::d("BFSGraph","duration: %d\n", duration);

    return 0;
}
